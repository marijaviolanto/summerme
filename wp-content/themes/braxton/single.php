<?php get_header(); ?>
	<div id="content-wrapper">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ($mvp_post_temp == "fullwidth") { ?>
		<?php } else { ?>
		<div id="content-main">
		<?php } ?>
			<?php $mvp_featured_img = get_option('mvp_featured_img'); if ($mvp_featured_img == "true") { ?>
				<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
					<?php echo get_post_meta($post->ID, "mvp_video_embed", true); ?>
				<?php else: ?>
					<?php $mvp_show_hide = get_post_meta($post->ID, "mvp_featured_image", true); if ($mvp_show_hide == "hide") { ?>
					<?php } else { ?>
						<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
						<div id="featured-image" itemscope itemtype="http://schema.org/Article">
							<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ($mvp_post_temp == "fullwidth") { ?>
								<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); ?>
							<?php } else { ?>
								<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); ?>
							<?php } ?>
							<img itemprop="image" src="<?php echo $thumb['0']; ?>" />
							<?php if(get_post_meta($post->ID, "mvp_photo_credit", true)): ?>
							<span class="photo-credit"><?php echo get_post_meta($post->ID, "mvp_photo_credit", true); ?></span>
							<?php endif; ?>
						</div><!--featured-image-->
						<?php } ?>
					<?php } ?>
				<?php endif; ?>
			<?php } ?>
			<div id="home-main">
				<div id="post-area" itemscope itemtype="http://schema.org/Article" <?php post_class(); ?>>
					<h3 class="story-cat"><strong class="single-brand"><?php echo rwmb_meta('_brand') ?></strong></h3>
					<h1 class="story-title" itemprop="name"><?php the_title(); ?></h1>
                     
                          <div class="media-product">
                            <div class="media-product-page">

                                <?php if (function_exists('rwmb_meta')) {
                                    // If Meta Box plugin is activate ?>
                                    <?php
                                    $youtubecode = rwmb_meta('anthemes_youtube', true);
                                    $vimeocode = rwmb_meta('anthemes_vimeo', true);
                                    $image = rwmb_meta('anthemes_slider', true);
                                    $hideimg = rwmb_meta('anthemes_hideimg', true);
                                    ?>

                                    <?php if (!empty($image)) { ?>
                                        <!-- #### Single Gallery #### -->
                                        <div class="single-gallery">

                                            <?php
                                            $images = rwmb_meta('anthemes_slider', 'type=image&size=thumbnail-gallery-single');
                                            foreach ($images as $key => $image) {
                                                echo wp_kses_post("<a href='{$image['full_url']}' title='{$image['caption']}' rel='mygallery'><img src='{$image['url']}'  alt='{$image['alt']}' width='{$image['width']}' height='{$image['height']}' /></a>");
                                            } ?>
                                        </div><!-- end .single-gallery -->
                                    <?php } ?>

                                    <?php if (!empty($youtubecode)) { ?>
                                        <!-- #### Youtube video #### -->
                                        <iframe class="single_iframe" width="720" height="420"
                                                src="//www.youtube.com/embed/<?php echo esc_html($youtubecode); ?>?wmode=transparent"
                                                frameborder="0" allowfullscreen></iframe>
                                    <?php } ?>

                                    <?php if (!empty($vimeocode)) { ?>
                                        <!-- #### Vimeo video #### -->
                                        <iframe class="single_iframe"
                                                src="//player.vimeo.com/video/<?php echo esc_html($vimeocode); ?>?portrait=0"
                                                width="720" height="420" frameborder="0" allowFullScreen></iframe>
                                    <?php } ?>

                                    <?php if (!empty($image) || !empty($youtubecode) || !empty($vimeocode)) { ?>
                                    <?php } elseif (has_post_thumbnail()) { ?>
                                        <?php if (!empty($hideimg)) {
                                        } else { ?>

                                        <?php } // disable featured image ?>
                                    <?php } ?>

                                    <a href="<?php echo rwmb_meta('_buyUrl') ?>" class="product-img-link" rel="nofollow" target="_blank"><img class="attachment-thumbnail-single-image wp-post-image"
                                         onerror="this.onerror=null;this.src='<?php echo get_stylesheet_directory_uri() . '/images/image-coming-soon.png' ?>';"
                                         src="<?php echo rwmb_meta('_image') ?>"/></a>

                                <?php } else {
                                    // Meta Box Plugin ?>

                                    <?php the_post_thumbnail('thumbnail-single-image'); ?>

                                <?php } ?>

                            </div><!-- end .media-product-page -->
                            <div class="media-product-details">
                                <div class="media-product-details-inner">
                                    <p class="product-price">
                                        <?php if (rwmb_meta('_salePrice')) { ?>
                                            <span class="old-price">
                                    Price <?php st_the_price() ?>
                                </span>
                                            <br>
                                            Sale! Only
                                            <span class="sale-price">
                                    <?php st_the_price(true) ?>
                                </span>
                                        <?php } else { ?>
                                            <span class="regular-price">
                                    Price <?php st_the_price() ?>
                                </span>
                                        <?php } ?>
                                    </p>
                                    <p class="product-content">
                                        <?php the_content() ?>
                                    </p>
                                    <p class="product-shipping">
                                        <?php echo rwmb_meta('_shipping') ?>
                                    </p>
                                    <div class="saving">
                                    <div class="arrow-down-related"></div>
                                    <div class="underline"></div>
                                    <h3><span itemprop="priceCurrency" content="<?php echo rwmb_meta('_currency') ?>">
                                            SAVE
                                            <?php if(rwmb_meta('_currency')=='USD')
                                            { 
                                                echo "$";
                                            }
                                            elseif(rwmb_meta('_currency')=='AUD') 
                                            {
                                                echo "aud";
                                            }
                                            else
                                            {
                                                echo rwmb_meta('_currency');
                                            } 
                                            ?>
                                           
                                            <?php echo rwmb_meta('_price')-rwmb_meta('_salePrice') ?>
                                        </span>
                                        <strong>NOW</strong>
                                    </h3>
                                </div>
                                    <p class="product-buy">
                                        <a href="<?php echo rwmb_meta('_buyUrl') ?>" target="_blank"
                                           class="btn-st btn-product-buy">I Want It <span class="buy-product-btn"></span></a>
                                    </p>
                                
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
					<?php $socialbox = get_option('mvp_social_box'); if ($socialbox == "true") { ?>
						
                    <div class="clear"></div>
                    <div class="entry text-center">
                        <!-- advertisement -->
                        <?php if (!empty($smof_data['ads_entry_top'])) { ?>
                            <div class="entry-img-300"><?php echo stripslashes($smof_data['ads_entry_top']); ?></div>
                        <?php } ?>
                        <!-- entry content -->


                        <?php wp_link_pages(); // content pagination ?>
                        <div class="clear"></div>
                        <!-- categories -->
                        <?php $categories = get_the_category();
                        if ($categories): ?>
                            <div class="ct-size">
                                <div class="product-categories"><?php esc_html_e('View more products like this', 'anthemes'); ?></div>
                                <span class="product-category" itemprop="category">
                                    <?php the_category(' &middot; '); ?>
                                </span>
                            </div>
                            <div class="clear"></div>
                        <?php endif; ?>

                        <div class="clear"></div>
                    </div><!-- end .entry -->
                    
					<?php } ?>
					
				</div><!--post-area-->
				<?php /*getRelatedPosts();*/ ?>
				<?php comments_template(); ?>
                <!--Mailchimp single-->
                        <div class="clear"></div>
                        <div class="mailchimp-single">
                            <p>Get <span><?php echo rwmb_meta('_brand') ?> </span>New Arrivals And Sales In Your Inbox.
                            </p>

                            <?php echo do_shortcode('[mailchimp action="//getlivetrends.us12.list-manage.com/subscribe/post?u=75662962e8434db020f430e44&amp;id=eeb28c7615" secure="b_75662962e8434db020f430e44_eeb28c7615" label="Join"]'); ?>

                        </div>

                        <!--Mailchimp-->
                <div class="clear"></div>
                <?php echo do_shortcode('[related_posts_by_tax posts_per_page="18" before_title=\'<h3 class="rpbt_title title">\' title="More Products You Will Love!" after_title="</h3>"]'); ?>
               
		</div>
             <div class="clear"></div>
                <a href="http://joyofsale.com/" target="_blank" class="single-banner"><div></div></a>
			</div><!--home-main-->
		<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ($mvp_post_temp == "fullwidth") { ?>
		<?php } else { ?>
            <!--content-main-->
		<?php get_sidebar(); ?>
		<?php } ?>
		<?php endwhile; endif; ?>
	</div><!--content-wrapper-->
</div><!--main-wrapper-->
<?php get_footer(); ?>