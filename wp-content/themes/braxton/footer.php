		<div id="footer-wrapper">
			<div id="footer-top">
				<div id="footer-nav">
					<?php wp_nav_menu(array('theme_location' => 'footer-menu')); ?>
				</div><!--footer-nav-->
				<?php if(get_option('mvp_footer_leader')) { ?>
				<div id="footer-leaderboard">
					<?php $ad970 = get_option('mvp_footer_leader'); if ($ad970) { echo stripslashes($ad970); } ?>
				</div><!--footer-leaderboard-->
				<?php } ?>
				<div id="footer-widget-wrapper">
					<?php $footer_info = get_option('mvp_footer_info'); if ($footer_info == "true") { ?>
					<?php } ?>
					<?php if ( ! dynamic_sidebar( 'footer-widget' ) ) : ?>
					<div class="footer-widget">
						<h3 class="footer-widget-header">Specially Chosen For You</h3>
						<ul class="home-list">
							<?php $recent = new WP_Query(array('posts_per_page' => '6' )); while($recent->have_posts()) : $recent->the_post();?>
							<li>
								<a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="home-list-content">
									<h2><?php the_title(); ?></h2>
								</div><!--home-list-content-->
								</a>
							</li>
							<?php endwhile; ?>
						</ul>
                        
                       
                        
					</div><!--footer-widget-->
					<?php endif; ?>
 					<?php if ( is_active_sidebar( 'footer-widget' ) ) : ?>
					<?php endif; ?>
                    <div class="clear"></div>
                     <div id="copyright">
							<p><?php echo get_option('mvp_copyright'); ?> <?php echo date('Y'); ?> <img src="../wp-content/themes/braxton/images/logos/footer-logo.png" alt="logosmall" /></p>
						</div><!--copyright-->
				</div><!--footer-widget-wrapper-->
			</div><!--footer-top-->
		</div><!--footer-wrapper-->
	</div><!--body-wrapper-->
</div><!--site-->

<!-- Piwik -->
<script type="text/javascript">
 var _paq = _paq || [];
 _paq.push(['trackPageView']);
 _paq.push(['enableLinkTracking']);
 (function() {
   var u="//www.violanto.com/rockin/";
   _paq.push(['setTrackerUrl', u+'piwik.php']);
   _paq.push(['setSiteId', 11]);
   var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
   g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
 })();
</script>
<noscript><p><img src="//www.violanto.com/rockin/piwik.php?idsite=11" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

<?php wp_footer(); ?>

</body>
</html>