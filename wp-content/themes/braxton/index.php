<?php get_header(); ?>
	<div id="content-wrapper">
		<div id="content-main">
			<div id="home-main">
            <?php if (is_home()) { ?>
                <div class="shortcut cf">

                    <div class="shortcut-child">
                        <a href="http://summerme.com/women/"><img
                                src="<?php bloginfo('stylesheet_directory'); ?>/images/home-cats-shoes-w250.png"
                                alt="women"/>
                            <h3>Women</h3></a>
                        <a href="http://summerme.com/dresses/"><p class="shortcut-text">Dresses</p></a>
                        <a href="http://summerme.com/jumpsuits/"><p class="shortcut-text">Jumpsuits</p></a>
                        <a href="http://summerme.com/swimwear/"><p class="shortcut-text">Swimwear</p>
                        </a>
                    </div>

                    <div class="shortcut-child">
                        <a href="http://summerme.com/shoes/"><img
                                src="<?php bloginfo('stylesheet_directory'); ?>/images/home-cats-boots-w250.png"
                                alt="pumps"/>
                            <h3>Shoes </h3></a>
                        <a href="http://summerme.com/heels/"><p class="shortcut-text">Pumps</p></a>
                        <a href="http://summerme.com/sneakers/"><p class="shortcut-text">Sneakers</p></a>
                        <a href="http://summerme.com/booties/"><p class="shortcut-text">Booties</p>
                        </a>
                    </div>

                    <div class="shortcut-child">
                        <a href="http://summerme.com/clothing/"><img
                                src="<?php bloginfo('stylesheet_directory'); ?>/images/home-cats--designers-w250.png"
                                alt="brands"/>
                            <h3>Brands </h3></a>
                        <a href="http://summerme.com/tory-burch/"><p class="shortcut-text">Tory Burch</p>
                        </a>
                        <a href="http://summerme.com/marc-jacobs/"><p class="shortcut-text">Marc Jacobs</p></a>
                        <a href="http://summerme.com/free-people/"><p class="shortcut-text">Free People </p>
                        </a>
                    </div>

                </div>
                <h1 class="designer-clothes"> Shop designer clothes and footwear by top brands</h1>
            <?php } ?>
                <div class="archive-header">
                    <?php include get_stylesheet_directory() . '/common/filter-frontend.php'; ?>
                </div>
				<div class="home-widget">
					<ul class="split-columns infinite-content">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<li class="infinite-post">
							<a href="<?php the_permalink(); ?>" rel="bookmark">
							<div class="home-list-img">
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
									<?php the_post_thumbnail('medium-thumb'); ?>
								<?php } ?>
								<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
									<div class="video-button">
										<img src="<?php echo get_template_directory_uri(); ?>/images/video-but.png" alt="<?php the_title(); ?>" />
									</div><!--video-button-->
								<?php endif; ?>
								<span class="widget-cat-contain"><h3 class="widget-cat">
                                <?php if (rwmb_meta('_salePrice')) { ?>
                                    <span class="old-price">
                            <?php st_the_price() ?>
                        </span>
                                    <span class="sale-price">
                            <?php st_the_price(true) ?>
                        </span>
                                <?php } else { ?>
                                    <span class="regular-price">
                            <?php st_the_price() ?>
                        </span>
                                <?php } ?>
                            </h3></span>
                                
                                <a href="<?php the_permalink(); ?>" class="product-img-link" rel="nofollow" target="_blank"><img onerror="this.onerror=null;this.src='<?php echo get_stylesheet_directory_uri() . '/images/image-coming-soon.png' ?>';" src="<?php echo rwmb_meta('_image') ?>" /></a>
                                
							</div><!--home-list-img-->
							<div class="home-list-content">
                                
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<!--<p><?php /*echo excerpt(19);*/ ?></p>-->
							</div><!--home-list-content-->
							</a>
                            <a href="<?php echo rwmb_meta('_buyUrl') ?>" target="_blank" class="btn-st" rel="nofollow"><div class="more">Take Me Home <span class="buy-product-btn"></span></div></a>
						</li>
						<?php endwhile; endif; ?>
					</ul>
				<div class="nav-links">
					<?php if (function_exists("pagination")) { pagination($wp_query->max_num_pages); } ?>
				</div><!--nav-links-->
				</div><!--home-widget-->
			</div><!--home-main-->
		</div><!--content-main-->
		<?php get_sidebar('home'); ?>
	</div><!--content-wrapper-->
</div><!--main-wrapper-->
<?php get_footer(); ?>