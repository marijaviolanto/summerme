<?php get_header(); ?>
	<div id="content-wrapper">
		<div id="content-main">
			<div id="home-main">
				<h1 class="home-widget-header">Shop <?php single_cat_title(); ?></h1>
                <div class="archive-header">
                    <?php include get_stylesheet_directory() . '/common/filter-frontend.php'; ?>
                </div>
				<?php if(get_option('mvp_category_layout') == 'large') { ?>
				<div class="home-widget">
					<ul class="split-columns cat-home-widget infinite-content">
						<?php $mvp_slider_cat = get_option('mvp_slider_cat'); if ($mvp_slider_cat == "true") { ?>
							<?php $current_category = single_cat_title("", false); $category_id = get_cat_ID($current_category); $cat_posts = new WP_Query(array('posts_per_page' => get_option('mvp_slider_cat_num'), 'cat' => $category_id )); while($cat_posts->have_posts()) : $cat_posts->the_post(); $do_not_duplicate[] = $post->ID; if (isset($do_not_duplicate)) { ?>
							<?php } endwhile; ?>

							<?php if (isset($do_not_duplicate)) { if (have_posts()) : while (have_posts()) : the_post(); if (in_array($post->ID, $do_not_duplicate)) continue; ?>
							<li class="infinite-post">
								<a href="<?php the_permalink(); ?>" target="_blank" rel="nofollow">
								<div class="split-img">
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<span class="wide-shade">
											<?php the_post_thumbnail('post-thumb'); ?>
										</span>
									<?php } ?>
									<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
										<div class="video-button">
											<img src="<?php echo get_template_directory_uri(); ?>/images/video-but.png" alt="<?php the_title(); ?>" />
										</div><!--video-button-->
									<?php endif; ?>
								</div><!--wide-img-->
                            </a>
                            <a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="split-text">
									<h2><?php the_title(); ?></h2>
								</div><!--wide-text-->
								</a>
                                <a href="<?php echo rwmb_meta('_buyUrl') ?>" target="_blank" class="btn-st" rel="nofollow"><div class="more">Take Me Home <span class="buy-product-btn"></span></div></a>
							</li>
							<?php endwhile; endif; } ?>
						<?php } else { ?>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<li class="infinite-post">
								<a href="<?php the_permalink(); ?>" target="_blank" rel="nofollow">
								<div class="split-img">
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<span class="wide-shade">
											<?php the_post_thumbnail('post-thumb'); ?>
										</span>
									<?php } ?>
									<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
										<div class="video-button">
											<img src="<?php echo get_template_directory_uri(); ?>/images/video-but.png" alt="<?php the_title(); ?>" />
										</div><!--video-button-->
									<?php endif; ?>
								</div><!--wide-img-->
                            </a>
                            <a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="split-text">
									<h2><?php the_title(); ?></h2>
									<span class="widget-info"><span class="widget-author"><?php the_author(); ?></span> | <?php the_time(get_option('date_format')); ?></span>
									<!--<p><?php /*echo excerpt(20);*/ ?></p>-->
								</div><!--wide-text-->
								</a>
                                <a href="<?php echo rwmb_meta('_buyUrl') ?>" target="_blank" class="btn-st" rel="nofollow"><div class="more">Take Me Home <span class="buy-product-btn"></span></div></a>
							</li>
							<?php endwhile; endif; ?>
						<?php } ?>
					</ul>
				<div class="nav-links">
					<?php if (function_exists("pagination")) { pagination($wp_query->max_num_pages); } ?>
				</div><!--nav-links-->
				</div><!--home-widget-->
				<?php } else if(get_option('mvp_category_layout') == 'list') { ?>
				<div class="home-widget">
					<ul class="split-columns cat-home-widget infinite-content">
						<?php $mvp_slider_cat = get_option('mvp_slider_cat'); if ($mvp_slider_cat == "true") { ?>
							<?php $current_category = single_cat_title("", false); $category_id = get_cat_ID($current_category); $cat_posts = new WP_Query(array('posts_per_page' => get_option('mvp_slider_cat_num'), 'cat' => $category_id )); while($cat_posts->have_posts()) : $cat_posts->the_post(); $do_not_duplicate[] = $post->ID; ?>
							<?php endwhile; ?>

							<?php if (isset($do_not_duplicate)) { if (have_posts()) : while (have_posts()) : the_post(); if (in_array($post->ID, $do_not_duplicate)) continue; ?>
							<li class="infinite-post">
								<a href="<?php the_permalink(); ?>" target="_blank" rel="nofollow">
								<div class="split-img">
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<?php the_post_thumbnail('medium-thumb'); ?>
									<?php } ?>
									<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
										<div class="video-button">
											<img src="<?php echo get_template_directory_uri(); ?>/images/video-but.png" alt="<?php the_title(); ?>" />
										</div><!--video-button-->
									<?php endif; ?>
                                    <span class="widget-cat-contain"><h3 class="widget-cat">
                                <?php if (rwmb_meta('_salePrice')) { ?>
                                    <span class="old-price">
                            <?php st_the_price() ?>
                        </span>
                                    <span class="sale-price">
                            <?php st_the_price(true) ?>
                        </span>
                                <?php } else { ?>
                                    <span class="regular-price">
                            <?php st_the_price() ?>
                        </span>
                                <?php } ?>
                            </h3></span>
                                
                                <img onerror="this.onerror=null;this.src='<?php echo get_stylesheet_directory_uri() . '/images/image-coming-soon.png' ?>';" src="<?php echo rwmb_meta('_image') ?>" />
								</div><!--home-list-img-->
                            </a>
                            <a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="split-text">
									<span class="widget-info">
									<h2><?php the_title(); ?></h2>
								</div><!--home-list-content-->
								</a>
                                <a href="<?php echo rwmb_meta('_buyUrl') ?>" target="_blank" class="btn-st" rel="nofollow"><div class="more">Take Me Home <span class="buy-product-btn"></span></div></a>
							</li>
							<?php endwhile; endif; } ?>
						<?php } else { ?>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<li class="infinite-post">
								<a href="<?php the_permalink(); ?>" target="_blank" rel="nofollow">
								<div class="split-img">
                                    <img onerror="this.onerror=null;this.src='<?php echo get_stylesheet_directory_uri() . '/images/image-coming-soon.png' ?>';" src="<?php echo rwmb_meta('_image') ?>" />
                                    <span class="widget-cat-contain"><h3 class="widget-cat">
                                <?php if (rwmb_meta('_salePrice')) { ?>
                                    <span class="old-price">
                            <?php st_the_price() ?>
                        </span>
                                    <span class="sale-price">
                            <?php st_the_price(true) ?>
                        </span>
                                <?php } else { ?>
                                    <span class="regular-price">
                            <?php st_the_price() ?>
                        </span>
                                <?php } ?>
                            </h3></span>
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<?php the_post_thumbnail('medium-thumb'); ?>
									<?php } ?>
									<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
										<div class="video-button">
											<img src="<?php echo get_template_directory_uri(); ?>/images/video-but.png" alt="<?php the_title(); ?>" />
										</div><!--video-button-->
									<?php endif; ?>
								</div><!--home-list-img-->
                            </a>
                            <a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="split-text">
									<span class="widget-info">
									<h2><?php the_title(); ?></h2>
								</div><!--home-list-content-->
								</a>
                                <a href="<?php echo rwmb_meta('_buyUrl') ?>" target="_blank" class="btn-st" rel="nofollow"><div class="more">Take Me Home <span class="buy-product-btn"></span></div></a>
							</li>
							<?php endwhile; endif; ?>
						<?php } ?>
					</ul>
				<div class="nav-links">
					<?php if (function_exists("pagination")) { pagination($wp_query->max_num_pages); } ?>
				</div><!--nav-links-->
				</div><!--home-widget-->
				<?php } else if(get_option('mvp_category_layout') == 'columns') { ?>
				<div class="home-widget">
					<ul class="split-columns cat-home-widget infinite-content">
						<?php $mvp_slider_cat = get_option('mvp_slider_cat'); if ($mvp_slider_cat == "true") { ?>
							<?php $current_category = single_cat_title("", false); $category_id = get_cat_ID($current_category); $cat_posts = new WP_Query(array('posts_per_page' => get_option('mvp_slider_cat_num'), 'cat' => $category_id )); while($cat_posts->have_posts()) : $cat_posts->the_post(); $do_not_duplicate[] = $post->ID; ?>
							<?php endwhile; ?>

							<?php if (isset($do_not_duplicate)) { if (have_posts()) : while (have_posts()) : the_post(); if (in_array($post->ID, $do_not_duplicate)) continue; ?>
							<li class="infinite-post">
								<a href="<?php the_permalink(); ?>" target="_blank" rel="nofollow">
								<div class="split-img">
                                    <img onerror="this.onerror=null;this.src='<?php echo get_stylesheet_directory_uri() . '/images/image-coming-soon.png' ?>';" src="<?php echo rwmb_meta('_image') ?>" />
                                    <span class="widget-cat-contain"><h3 class="widget-cat">
                                <?php if (rwmb_meta('_salePrice')) { ?>
                                    <span class="old-price">
                            <?php st_the_price() ?>
                        </span>
                                    <span class="sale-price">
                            <?php st_the_price(true) ?>
                        </span>
                                <?php } else { ?>
                                    <span class="regular-price">
                            <?php st_the_price() ?>
                        </span>
                                <?php } ?>
                            </h3></span>
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<?php the_post_thumbnail('medium-thumb'); ?>
									<?php } ?>
									<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
										<div class="video-button">
											<img src="<?php echo get_template_directory_uri(); ?>/images/video-but.png" alt="<?php the_title(); ?>" />
										</div><!--video-button-->
									<?php endif; ?>
								</div><!--split-img-->
                            </a>
                            <a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="split-text">
									<h2><?php the_title(); ?></h2>
								</div><!--split-text-->
								</a>
                                <a href="<?php echo rwmb_meta('_buyUrl') ?>" target="_blank" class="btn-st" rel="nofollow"><div class="more">Take Me Home <span class="buy-product-btn"></span></div></a>
							</li>
							<?php endwhile; endif; } ?>
						<?php } else { ?>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<li class="infinite-post">
								<a href="<?php the_permalink(); ?>" target="_blank" rel="nofollow">
								<div class="split-img">
                                     <img onerror="this.onerror=null;this.src='<?php echo get_stylesheet_directory_uri() . '/images/image-coming-soon.png' ?>';" src="<?php echo rwmb_meta('_image') ?>" />
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<?php the_post_thumbnail('medium-thumb'); ?>
									<?php } ?>
									<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
										<div class="video-button">
											<img src="<?php echo get_template_directory_uri(); ?>/images/video-but.png" alt="<?php the_title(); ?>" />
                                            <span class="widget-cat-contain"><h3 class="widget-cat">
                                <?php if (rwmb_meta('_salePrice')) { ?>
                                    <span class="old-price">
                            <?php st_the_price() ?>
                        </span>
                                    <span class="sale-price">
                            <?php st_the_price(true) ?>
                        </span>
                                <?php } else { ?>
                                    <span class="regular-price">
                            <?php st_the_price() ?>
                        </span>
                                <?php } ?>
                            </h3></span>
										</div><!--video-button-->
									<?php endif; ?>
								</div><!--split-img-->
                            </a>
                            <a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="split-text">
									<h2><?php the_title(); ?></h2>
                                    
								</div><!--split-text-->
								</a>
                                <a href="<?php echo rwmb_meta('_buyUrl') ?>" target="_blank" class="btn-st" rel="nofollow"><div class="more">Take Me Home <span class="buy-product-btn"></span></div></a>
							</li>
							<?php endwhile; endif; ?>
						<?php } ?>
					</ul>
				<div class="nav-links">
					<?php if (function_exists("pagination")) { pagination($wp_query->max_num_pages); } ?>
				</div><!--nav-links-->
				</div><!--home-widget-->
				<?php } ?>
			</div><!--home-main-->
		</div><!--content-main-->
		<?php get_sidebar(); ?>
	</div><!--content-wrapper-->
</div><!--main-wrapper-->
<?php get_footer(); ?>