<form id="searchform2" class="header-search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input placeholder="<?php esc_html_e('Live Search ...', 'anthemes'); ?>" type="text" name="s" id="s" />
    <input type="submit" value="Search" class="buttonicon" />
</form><div class="clear"></div>