<?php
/*
Template Name: Template - Default with Sidebar and Children
*/
?>
<?php get_header(); // add header ?>

<!-- Begin Content -->
<div class="wrap-fullwidth">

    <div class="single-content">

        <!-- ads -->
        <?php if (!empty($smof_data['header_728'])) { ?>
            <div class="single-box">
                <div class="single-money">
                    <?php echo stripslashes($smof_data['header_728']); ?>
                </div>
            </div>
            <div class="clear"></div>
        <?php } ?>

        <article>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">

                    <div class="entry">
                        <h1 class="page-title"><?php the_title(); ?></h1>
                        <?php the_content(''); // content ?>
                        <?php wp_link_pages(); // content pagination ?>
                        <div class="clear"></div>
                        <br/>
                    </div><!-- end #entry -->
                </div><!-- end .post -->
            <?php endwhile; endif; ?>
        </article>

        <?php $pages = get_pages([
            'child_of' => $post->ID,
            'sort_column' => 'menu_order, post_title'

        ]); ?>
        <div class="content-bgd">
            <div class="entry">
                <ul class="child-pages">
                    <?php foreach ($pages as $post): setup_postdata($post); ?>
                        <li class="cf">
                            <a href="<?php the_permalink(); ?>"  rel="nofollow"
                               class="child-image cf"> <?php the_post_thumbnail(); ?> </a>

                            <h3>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h3>

                            <p><?php echo anthemes_excerpt(strip_tags(strip_shortcodes(get_the_excerpt())), 200); ?><br>
			    <a href="<?php the_permalink(); ?>" rel="nofollow">Read more</a></p>
                        </li>
                    <?php endforeach; ?>
                </ul>

            </div>
        </div>


    </div><!-- end .single-content -->

    <!-- Begin Sidebar (right) -->
    <?php get_sidebar(); // add sidebar ?>
    <!-- end #sidebar  (right) -->

    <div class="clear"></div>
</div><!-- end .wrap-fullwidth -->

<?php get_footer(); // add footer  ?>