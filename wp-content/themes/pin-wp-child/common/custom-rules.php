<?php

add_action('pre_get_posts', 'apply_custom_rules');

function apply_custom_rules($query)
{

    if ((is_category() || is_archive()) && $query->is_main_query()) {

        if (get_query_var('order')){

            $metaQuery = array(
                'relation' => 'AND',
                '_price' => array(
                    'key' => '_price',
                    'type' => 'NUMERIC',
                    'compare' => 'EXISTS',
                ),
                '_salePrice' => array(
                    'key' => '_salePrice',
                    'type' => 'NUMERIC',
                    'compare' => 'EXISTS',
                ),
            );
            $query->set('meta_query', $metaQuery);

            $query->query_vars['orderby'] = array(
                '_salePrice' => get_query_var('order', 'ASC'),
                '_price' => get_query_var('order', 'ASC')
            );

        }

    }

}
