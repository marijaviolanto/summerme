(function ($) {

    $(function () {

        var $form = $('.js-filter-form');
        var initialData = $form.serialize();

        var min = $form.data('min');
        var max = $form.data('max');

        function updateOrder() {
            $('.order-placeholder').text('Order by ' + $('[name=order]:checked').parent().text());
            highlightButton();
        }

        $('[name=order]').change(updateOrder);
        updateOrder();

        $('.search-form-brands-checkbox').change(function () {
            highlightButton()
        });


        var $rangeSlider = $("#slider-range-price");

        $rangeSlider.slider({
            range: true,
            min: min,
            max: max,
            values: [
                $('[name=price_low]').val() || min,
                $('[name=price_high]').val() || max
            ],
            slide: function (event, ui) {

                $('.filter-amount').html('$' + ui.values[0] +
                    ' - ' + (ui.values[1] == max ? 'Maximum' : '$' + ui.values[1]));
            },
            change: function (event, ui) {

                $('input[name=price_low]').val(ui.values[0]);
                $('input[name=price_high]').val(ui.values[1]);

                $('.filter-amount').html('$' + ui.values[0] +
                    ' - ' + (ui.values[1] == max ? 'Maximum' : '$' + ui.values[1]));

                highlightButton()
            }
        });


        $('.filter-amount').html('$' + $rangeSlider.slider('values', 0) +
            ' - ' + ($rangeSlider.slider('values', 1) == max ? 'Maximum' : '$' + $rangeSlider.slider('values', 1)));


        // end select box

        // sticky scroll
        $filters = $('.sticky-filter');

        if ($filters.length) {
            $(document).scroll(function () {
                var offsetTop = $filters.offset().top;

                if ($(document).scrollTop() > offsetTop) {
                    $filters.addClass('on');
                }
                else {
                    $filters.removeClass('on');
                }
            });
        }


        function highlightButton() {
            $('.filter-apply').toggleClass('active', initialData != $form.serialize());
        }

        // Brand functionality
        var $brandPlaceholder = $('.brand-placeholder');

        function updateBrandsSelect() {
            var $selected = $('.search-form-brands-checkbox:checked');
            var n = $selected.length
            if (n == 0) {
                $brandPlaceholder.text('Showing all brands');
                return;
            }
            if (n == 1) {
                $brandPlaceholder.text($selected.prop('title') + ' selected');
                return;
            }
            $brandPlaceholder.text(n + ' brands selected');
        }

        updateBrandsSelect();
        $('.search-form-brands-checkbox').change(updateBrandsSelect);
        // end Brand


        // select box
        $('.search-form-select').each(function () {
            var $select = $(this);
            var $dropdown = $select.find('.search-form-dropdown');
            var $placeholder = $select.find('.search-form-placeholder');

            $('body').click(function () {
                $dropdown.hide();
            });

            $dropdown.click(function () {
                if ($select.data('autoClose')) {
                    $dropdown.hide();
                }
            });

            $select.click(function (e) {
                e.stopPropagation();
            });
            $placeholder.click(function () {
                $('.search-form-dropdown').not($dropdown).hide();
                $dropdown.toggle();
            });
            $dropdown.scroll(function (e) {
                // TODO prevent scroll
            });

        });
    });
})(jQuery);
