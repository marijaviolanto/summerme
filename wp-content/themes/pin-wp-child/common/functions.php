<?php

include_once 'ratings.php';
include_once 'filter.php';
//
include_once 'reporting.php';


function get_input($key, $default = null, $allowEmpty = true)
{
    if (!$allowEmpty && isset($_REQUEST[$key]) && empty($_REQUEST[$key])) {
        return $default;
    }
    if (isset($_REQUEST[$key])) {
        return $_REQUEST[$key];
    }
    return $default;
}

function dd()
{
    if (!WP_DEBUG) return;
    foreach (func_get_args() as $arg) {
        var_dump($arg);
    }
    die();
}

add_action('wp_enqueue_scripts', 'theme_enqueue_styles');

function theme_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');

}

function st_the_price($salePrice = false)
{
    $currency = strtolower(rwmb_meta('_currency'));
    if ($currency == 'usd') {
        echo '$' . number_format(rwmb_meta($salePrice ? '_salePrice' : '_price'), 2);
        return;
    }
    echo number_format(rwmb_meta($salePrice ? '_salePrice' : '_price'), 2) . ' ' . strtolower(rwmb_meta('_currency'));
}

// Slide in filter

function slide_in_filter($content)
{

    $max = 7;
    $min = 1;

    $brand = get_post_meta(get_the_ID(), '_brand', true);

    // random seed is based on current brand, date and changes on each 30 secs
    srand(crc32($brand) . date('mdi') . round(date('s') / 30));
    $i = rand(1, 10);
    $number = ceil(pow(0.9, $i) * ($max - $min));

    $href = esc_url(get_category_link(get_cat_ID($brand)));

    $category_link = "<a href=\"$href\" title=\"$brand\">$brand</a>";

    return sprintf("%d %s shopping %s right now.\n%s",
        $number,
        _n('more person is', 'people are', $number),
        $category_link,
        $content
    );

}

add_filter('wdsi_content', 'slide_in_filter');

add_shortcode('mailchimp', function ($attrs) {

    ob_start();
    include 'mailchimp.php';
    return ob_get_clean();
});


// popups
add_action('wp_enqueue_scripts', 'theme_enqueue_popup_scripts');

function theme_enqueue_popup_scripts()
{
    wp_enqueue_script('jquery-ui-dialog');
}
