<?php get_header(); // add header ?>
<?php
// Options from admin panel
global $smof_data;

?>


    <!-- Begin Content -->
    <div class="wrap-fullwidth">
        <div class="single-content">
            <!-- ads -->
            <?php if (!empty($smof_data['header_728'])) { ?>
                <div class="single-box">
                    <div class="single-money">
                        <?php echo stripslashes($smof_data['header_728']); ?>
                    </div>
                </div>
                <div class="clear"></div>
            <?php } ?>
            <!-- end .single-box -->


            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="entry-top">
                    <strong class="single-brand"><?php echo rwmb_meta('_brand') ?></strong>
                    <h1 class="article-title entry-title"><?php the_title(); ?></h1>
                </div>
                <div class="clear"></div>
            <?php endwhile; endif; ?>


            <article>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php setPostViews_anthemes(get_the_ID()); ?>
                    <div <?php post_class('post') ?> id="post-<?php the_ID(); ?>">
                        <div class="media-product">
                            <div class="media-product-page">

                                <?php if (function_exists('rwmb_meta')) {
                                    // If Meta Box plugin is activate ?>
                                    <?php
                                    $youtubecode = rwmb_meta('anthemes_youtube', true);
                                    $vimeocode = rwmb_meta('anthemes_vimeo', true);
                                    $image = rwmb_meta('anthemes_slider', true);
                                    $hideimg = rwmb_meta('anthemes_hideimg', true);
                                    ?>

                                    <?php if (!empty($image)) { ?>
                                        <!-- #### Single Gallery #### -->
                                        <div class="single-gallery">

                                            <?php
                                            $images = rwmb_meta('anthemes_slider', 'type=image&size=thumbnail-gallery-single');
                                            foreach ($images as $key => $image) {
                                                echo wp_kses_post("<a href='{$image['full_url']}' title='{$image['caption']}' rel='mygallery'><img src='{$image['url']}'  alt='{$image['alt']}' width='{$image['width']}' height='{$image['height']}' /></a>");
                                            } ?>
                                        </div><!-- end .single-gallery -->
                                    <?php } ?>

                                    <?php if (!empty($youtubecode)) { ?>
                                        <!-- #### Youtube video #### -->
                                        <iframe class="single_iframe" width="720" height="420"
                                                src="//www.youtube.com/embed/<?php echo esc_html($youtubecode); ?>?wmode=transparent"
                                                frameborder="0" allowfullscreen></iframe>
                                    <?php } ?>

                                    <?php if (!empty($vimeocode)) { ?>
                                        <!-- #### Vimeo video #### -->
                                        <iframe class="single_iframe"
                                                src="//player.vimeo.com/video/<?php echo esc_html($vimeocode); ?>?portrait=0"
                                                width="720" height="420" frameborder="0" allowFullScreen></iframe>
                                    <?php } ?>

                                    <?php if (!empty($image) || !empty($youtubecode) || !empty($vimeocode)) { ?>
                                    <?php } elseif (has_post_thumbnail()) { ?>
                                        <?php if (!empty($hideimg)) {
                                        } else { ?>

                                            <?php // the_post_thumbnail('thumbnail-single-image'); ?>
                                            <!-- <img class="attachment-thumbnail-single-image wp-post-image"
                             src="<?php // echo rwmb_meta('_image')?>" /> -->

                                        <?php } // disable featured image ?>
                                    <?php } ?>

                                    <img class="attachment-thumbnail-single-image wp-post-image"
                                         onerror="this.onerror=null;this.src='<?php echo get_stylesheet_directory_uri() . '/images/image-coming-soon.png' ?>';"
                                         src="<?php echo rwmb_meta('_image') ?>"/>

                                <?php } else {
                                    // Meta Box Plugin ?>

                                    <?php the_post_thumbnail('thumbnail-single-image'); ?>

                                <?php } ?>

                            </div><!-- end .media-product-page -->
                            <div class="media-product-details">
                                <div class="media-product-details-inner">
                                    <p class="product-price">
                                        <?php if (rwmb_meta('_salePrice')) { ?>
                                            <span class="old-price">
                                    Price <?php st_the_price() ?>
                                </span>
                                            <br>
                                            Sale! Only
                                            <span class="sale-price">
                                    <?php st_the_price(true) ?>
                                </span>
                                        <?php } else { ?>
                                            <span class="regular-price">
                                    Price <?php st_the_price() ?>
                                </span>
                                        <?php } ?>
                                    </p>
                                    <p class="product-content">
                                        <?php the_content() ?>
                                    </p>
                                    <p class="product-shipping">
                                        <?php echo rwmb_meta('_shipping') ?>
                                    </p>
                                    <p>
                                        <a href="<?php echo rwmb_meta('_buyUrl') ?>" target="_blank"
                                           class="btn-st btn-product-buy">Buy This product</a>
                                    </p>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="single-share">
                            <?php $video_wp_facebooklink = 'https://www.facebook.com/sharer/sharer.php?u='; ?>
                            <a class="fbbutton" target="_blank"
                               href="<?php echo esc_url($video_wp_facebooklink); ?><?php the_permalink(); ?>&amp;=<?php the_title(); ?>"><i
                                    class="fa fa-facebook-official"></i>
                                <span><?php esc_html_e('Share', 'video_wp'); ?></span></a>
                            <?php $video_wp_twitterlink = 'https://twitter.com/home?status=Check%20out%20this%20article:%20'; ?>
                            <a class="twbutton" target="_blank"
                               href="<?php echo esc_url($video_wp_twitterlink); ?><?php the_title(); ?>%20-%20<?php the_permalink(); ?>"><i
                                    class="fa fa-twitter"></i>
                                <span><?php esc_html_e('Tweet', 'video_wp'); ?></span></a>
                            <?php $articleimage = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
                            <?php $video_wp_pinlink = 'https://pinterest.com/pin/create/button/?url='; ?>
                            <a class="pinbutton" target="_blank"
                               href="<?php echo esc_url($video_wp_pinlink); ?><?php the_permalink(); ?>&amp;media=<?php echo esc_html(rwmb_meta('_image')); ?>&amp;description=<?php the_title(); ?>"><i
                                    class="fa fa-pinterest"></i> <span><?php esc_html_e('Pinit', 'video_wp'); ?></span></a>
                            <?php $video_wp_googlelink = 'https://plus.google.com/share?url='; ?>
                            <a class="googlebutton" target="_blank"
                               href="<?php echo esc_url($video_wp_googlelink); ?><?php the_permalink(); ?>"><i
                                    class="fa fa-google-plus-square"></i>
                                <span><?php esc_html_e('Google+', 'video_wp'); ?></span></a>
                            <?php $video_wp_emaillink = 'mailto:?subject='; ?>
                            <a class="emailbutton" target="_blank"
                               href="<?php echo esc_url($video_wp_emaillink); ?><?php the_title(); ?>&amp;body=<?php the_permalink(); ?> <?php echo anthemes_excerpt(strip_tags(strip_shortcodes(get_the_excerpt())), 140); ?>"><i
                                    class="fa fa-envelope"></i>
                                <span><?php esc_html_e('Email', 'video_wp'); ?></span></a>
                        </div><!-- end #single-share -->
                        <div class="entry text-center">
                            <!-- advertisement -->
                            <?php if (!empty($smof_data['ads_entry_top'])) { ?>
                                <div
                                    class="entry-img-300"><?php echo stripslashes($smof_data['ads_entry_top']); ?></div>
                            <?php } ?>
                            <!-- entry content -->


                            <?php wp_link_pages(); // content pagination ?>
                            <div class="clear"></div>

                            <!-- tags -->
                            <?php /*$tags = get_the_tags();
                        if ($tags): ?>
                            <div class="ct-size"><div class="entry-btn"><?php esc_html_e( 'Article Tags:', 'anthemes' ); ?></div> <?php the_tags('', ' &middot; '); // tags ?></div><div class="clear"></div>
                        <?php endif; */ ?>

                            <!-- categories -->
                            <?php $categories = get_the_category();
                            if ($categories): ?>
                                <div class="ct-size">
                                    <div
                                        class="product-categories"><?php esc_html_e('View more products like this', 'anthemes'); ?></div>
                                    <?php the_category(' &middot; '); // categories ?></div>
                                <div class="clear"></div>
                            <?php endif; ?>

                            <div class="clear"></div>
                        </div><!-- end .entry -->

                        <!--Mailchimp single-->
                        <div class="clear"></div>
                        <div class="mailchimp-single">
                            <p>Get <span><?php echo rwmb_meta('_brand') ?> </span>New Arrivals And Sales In Your Inbox.
                            </p>

                            <?php echo do_shortcode('[mailchimp action="//getlivetrends.us12.list-manage.com/subscribe/post?u=75662962e8434db020f430e44&amp;id=af26b3be37" secure="b_75662962e8434db020f430e44_af26b3be37" ]'); ?>


                        </div>

                        <!--Mailchimp-->

                        <!--</div>-->

                        <div class="clear"></div>
                    </div><!-- end #post -->
                <?php endwhile; endif; ?>
            </article><!-- end article -->


            <!-- ads -->
            <?php if (!empty($smof_data['bottom728'])) { ?>
                <div class="single-728">
                    <div class="img728">
                        <?php echo stripslashes($smof_data['bottom728']); ?>
                    </div>
                </div>
            <?php } ?>


            <!-- author -->
            <?php if (get_the_author_meta('description')): ?>
                <div class="author-meta">
                    <div class="author-left-meta">
                        <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php echo get_avatar(get_the_author_meta('user_email'), 70); ?></a>
                        <ul class="author-social-top">
                            <?php if (get_the_author_meta('facebook')) { ?>
                                <li class="facebook"><a target="_blank"
                                                        href="//facebook.com/<?php echo esc_html(the_author_meta('facebook')); ?>"><i
                                        class="fa fa-facebook"></i></a></li><?php } ?>
                            <?php if (get_the_author_meta('twitter')) { ?>
                                <li class="twitter"><a target="_blank"
                                                       href="//twitter.com/<?php echo esc_html(the_author_meta('twitter')); ?>"><i
                                        class="fa fa-twitter"></i></a></li><?php } ?>
                            <?php if (get_the_author_meta('google')) { ?>
                                <li class="google"><a target="_blank"
                                                      href="//plus.google.com/<?php echo esc_html(the_author_meta('google')); ?>?rel=author"><i
                                        class="fa fa-google-plus"></i></a></li><?php } ?>
                        </ul>
                    </div><!-- end .author-left-meta -->
                    <div class="author-info">
                        <strong><?php the_author_posts_link(); ?></strong> &rsaquo; <a class="author-link"
                                                                                       href="<?php the_author_meta('url'); ?>"
                                                                                       target="_blank"><?php the_author_meta('url'); ?></a><br/>
                        <p><?php the_author_meta('description'); ?></p>
                    </div><!-- end .autor-info -->
                    <div class="clear"></div>
                </div><!-- end .author-meta -->
            <?php endif; ?>


            <!-- Recent and related Articles was here-->
            <!--<div class="related-box">-->
            <!-- Recent -->
            <!-- <div class="one_half">
            <h3 class="title"><?php esc_html_e('Recent Articles', 'anthemes'); ?></h3><div class="arrow-down-related"></div><div class="clear"></div>
            <ul class="article_list">
            <?php $anposts = new WP_Query(array('post_type' => 'post', 'ignore_sticky_posts' => 1, 'posts_per_page' => 4)); // number to display more / less ?>
            <?php while ($anposts->have_posts()) : $anposts->the_post(); ?>

              <li>
                <?php if (has_post_thumbnail()) { ?>
                  <div class="article-comm"><?php comments_popup_link('<i class="fa fa-comments"></i> 0', '<i class="fa fa-comments"></i> 1', '<i class="fa fa-comments"></i> %'); ?></div>
                  <a href="<?php the_permalink(); ?>"> <?php echo the_post_thumbnail('thumbnail-widget-small'); ?> </a>
                <?php } ?>
                  <div class="an-widget-title" <?php if (has_post_thumbnail()) { ?> style="margin-left:70px;" <?php } ?>>
                    <h4 class="article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                      <?php if (function_exists('taqyeem_get_score')) { ?>
                        <?php taqyeem_get_score(); ?>
                      <?php } ?>
                    <span><?php esc_html_e('by', 'anthemes'); ?> <?php the_author_posts_link(); ?></span>
                  </div>
              </li>

            <?php endwhile;
            wp_reset_query(); ?>
            </ul>
            </div>-->
            <!-- end .one_half Recent -->

            <!-- Related -->
            <!--<div class="one_half_last">
            <h3 class="title"><?php esc_html_e('Related Articles', 'anthemes'); ?></h3><div class="arrow-down-related"></div><div class="clear"></div>
            <ul class="article_list">
                <?php
            $orig_post = $post;
            global $post;
            $tags = wp_get_post_tags($post->ID);
            if ($tags) {
                $tag_ids = array();
                foreach ($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
                $args = array(
                    'tag__in' => $tag_ids,
                    'post__not_in' => array($post->ID),
                    'posts_per_page' => 4, // Number of related posts to display.
                    'ignore_sticky_posts' => 1
                );
                $my_query = new wp_query($args);
                while ($my_query->have_posts()) {
                    $my_query->the_post();
                    ?>

              <li>
                <?php if (has_post_thumbnail()) { ?>
                  <div class="article-comm"><?php comments_popup_link('<i class="fa fa-comments"></i> 0', '<i class="fa fa-comments"></i> 1', '<i class="fa fa-comments"></i> %'); ?></div>
                  <a href="<?php the_permalink(); ?>"> <?php echo the_post_thumbnail('thumbnail-widget-small'); ?> </a>
                <?php } ?>
                  <div class="an-widget-title" <?php if (has_post_thumbnail()) { ?> style="margin-left:70px;" <?php } ?>>
                    <h4 class="article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                      <?php if (function_exists('taqyeem_get_score')) { ?>
                        <?php taqyeem_get_score(); ?>
                      <?php } ?>
                    <span><?php esc_html_e('by', 'anthemes'); ?> <?php the_author_posts_link(); ?></span>
                  </div>
              </li>

            <?php }
            }
            $post = $orig_post;
            wp_reset_query(); ?>
            </ul>
            </div>-->
            <!-- end .one_half_last Related -->
            <!-- <div class="clear"></div>
         </div>-->
            <!-- end .related-box -->
            <?php echo do_shortcode('[related_posts_by_tax posts_per_page="18" before_title=\'<h3 class="rpbt_title title">\' title="More Products You Will Love!" after_title="</h3>"]'); ?>

        </div><!-- end .single-content -->


        <!-- Begin Sidebar (right) -->
        <?php get_sidebar(); // add sidebar ?>
        <!-- end #sidebar  (right) -->

        <div class="clear"></div>

        <!--banner-->
        <a target='new'
           href="http://click.linksynergy.com/fs-bin/click?id=9LaR6VyOH9Q&offerid=391503.167&subid=0&type=4"
           class="red-banner"><IMG border="0" alt="Lord & Taylor"
                                   src="http://ad.linksynergy.com/fs-bin/show?id=9LaR6VyOH9Q&bids=391503.167&subid=0&type=4&gridnum=0"></a>
        <!--end banner-->

    </div><!-- end .wrap-fullwidth  -->

<?php get_footer(); // add footer  ?>
